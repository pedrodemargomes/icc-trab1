#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>


/**
 * @brief      Calcula a norma euclidiana de um vetor
 *
 * @param      v     Vetor que será calculado a normal
 * @param[in]  size  Tamanho do vetor
 *
 * @return     A norma euclidiana do vetor.
 */
double normaVetor(double v[], int size){
	int i;
	double acumulador=0;
	for(i=0;i<size;++i)
		acumulador += v[i]*v[i];

	return sqrt(acumulador);
}


/**
 * @brief      Função que retorna uma timestamp para medir tempo de execução das funções.
 *
 * @return     Tempo percorrido em segundos
 */
double timestamp(void){
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return((double)(tp.tv_sec*1000.0 + tp.tv_usec/1000.0));
}


/**
 * @brief      Gera uma matrix quadrada aleatória de tamanho \p n
 *
 * @param      m     Ponteiro para espaço de memória em que será gerado a matriz
 * @param[in]  n     Tamanho da
 */
void generateSquareRandomMatrix(double **m ,unsigned int n ) {
	srand( 20172 );

	double x = 1.0/(double)RAND_MAX;

	int i,j;
	for(i=0;i<n;i++)
		for(j=0;j<n;j++)
			m[i][j] = rand()*x;
}


/**
 * @brief      Copia uma matriz
 *
 * @param      a     Matriz a ser copiada
 * @param      b     Matriz de destino
 * @param[in]  n     Tamanho da matriz a ser copiada
 */
void copiaMatriz(double **a,double **b,int n) {
	int i,j;
	for(i=0; i<n; i++) {
		for(j=0; j<n; j++) {
			a[i][j] = b[i][j];	
		}
	}
}


/**
 * @brief      { function_description }
 *
 * @param      out   Aquivo de saída dos dados a serem impressos
 * @param      x     { parameter_description }
 * @param[in]  n     { parameter_description }
 */
void imprimeResposta(FILE *out, double *x,int n) {
	int i;
	for(i = 0;i<n;i++)
		fprintf(out,"X %d = %.17g\n",i,x[i]);
}


/**
 * @brief      Imprime uma matriz na tela
 *
 * @param      out   Aquivo de saída dos dados a serem impressos
 * @param      m     Ponteiro para espaço de memória em que está a matriz a ser impressa
 * @param[in]  n     Tamanho da matriz a ser impressa
 */
void imprimeMatriz(FILE *out, double **m,int n) {
	int i,j;
	for(i=0; i<n; i++) {
		for(j=0; j<n-1; j++) {
			fprintf(out,"%.17g ",m[i][j]);	
		}
		fprintf(out,"%.17g\n",m[i][n-1]);	
	}
}


/**
 * @brief      Lê uma matriz para a memória
 *
 * @param      in    Arquivo de entrada dos dados
 * @param      a     Espaço de memória em que a matriz será lida
 * @param[in]  n     Tamanho da matriz a ser lida
 */
void leMatriz(FILE *in, double **a,int n) {
	int i,j;
	for(i = 0;i<n;i++) {
		for(j= 0;j<n;j++) {
			fscanf(in,"%lf",&a[i][j]);
		}
	}	
}


/**
 * @brief      Aloca um espaço de memória para uma matriz de tamanho \p n
 *
 * @param[in]  n     Tamanho da matriz
 *
 * @return     Ponteiro para o novo espaço de memória alocado
 */
double **alocaMatriz(int n) {
	int i;
	double **a = (double **)malloc(n*sizeof(double *));
	for(i = 0;i < n; i++) {
		a[i] = (double *)malloc(n*sizeof(double));
	}
	return a;
}



/**
 * @brief      Troca as linhas da matriz entre sí nas posições \p y e \p t
 *
 * @param      a     Ponteiro para endereço de memória que está a matriz a ter as linhas trocadas
 * @param[in]  y     Posição da primeira linha
 * @param[in]  t     Posição da segunda linha
 * @param[in]  n     Largura da matriz
 */
inline void trocaLinhas(double **a,int y,int t,int n) {
	int i;
	double tmp;
	for(i=0;i<n;i++) {
		tmp = a[y][i];
		a[y][i] = a[t][i];
		a[t][i] = tmp;
	}
}


/**
 * @brief      Multiplica a matriz \p a pela matriz \p b
 *
 * @param      a     Ponteiro para a primeira matriz
 * @param      b     Ponteiro para a segunda matriz
 * @param[in]  n     Tamanho das matrizes
 *
 * @return     Ponteiro para endereço de memória em que está \p a x \p b
 */
double **multiplicaMatriz(double **a,double **b,int n) {
	int i,j,k;
	double soma;
	double **r = alocaMatriz(n);
	
	for(i=0;i<n;i++) {
		for(j=0;j<n;j++) {
			soma = 0;
			for(k=0;k<n;k++)
				soma += a[i][k]*b[k][ j ];

			r[i][j] = soma;
		}
		
	}
	return r;
}


/**
 * @brief      Não sei que porra é essa arruma ai pedro
 *
 * @param      a     { parameter_description }
 * @param[in]  i     { parameter_description }
 * @param[in]  j     { parameter_description }
 *
 * @return     { description_of_the_return_value }
 */
inline double L(double **a,int i ,int j) {
	if( i == j )
		return 1.0;
	else if(i > j)
		return a[i][j];
	else
		return 0.0;
}


/**
 * @brief      Lê a entrada padrão do programa
 *
 * @param[in]  argc  Número de argumentos
 * @param      argv  Vetor com os argumentos
 * @param      in    Ponteiro para o arquivo de entrada que será utilizado pelo programa
 * @param      out   Ponteiro para o arquivo de saída que será utilizado pelo programa
 * @param      n     Ponteiro para o inteiro que será usado para definir o tamanho da matriz aleatória a ser gerada
 * @param      k     Ponteiro para o inteiro que será usado para definir o número de iterações do refinamento a serem executadas
 */
void leEntrada(int argc,char *argv[],FILE **in,FILE **out,int *n, int *k) {
	*in = stdin;
	*out = stdout;

	int i;
	for(i=1;i<argc;i++) {
		if(argv[i][0] == '-' ) {		
			switch(argv[i][1]) {
				case 'i':
					*in = fopen(argv[i+1], "r");
				break;
				case 'o':
					*out = fopen(argv[i+1],"w+");
				break;
				case 'r':
					*n = atoi(argv[i+1]);
				break;
				case 'e':
					*k = atoi(argv[i+1]);
				break;
				default:
				break;
			}
		}
	}
}


/**
 * @brief      Faz o metodo de Gauss com pivotamento na matriz \p a[][] de tamanho \p n e armazena 
as trocas de linha no vetor \p ordemLinhas[]
 *
 * @param      a            Ponteiro para a matriz que será operada
 * @param[in]  n            Tamanho da matriz
 * @param      ordemLinhas  Ponteiro para vetor que terá a ordem das linhas após o pivotamento
 */
void metodoDeGaussPivotamento(double **a,int n,int *ordemLinhas) {
	int i,j,v,k,tmp,maior;
	double m;
	for(i = 0;i<n;i++) {
		// ++ Pivotamento parcial ++
		maior = i;
		for(v=i+1;v<n;v++) {
			if( fabs(a[maior][i]) < fabs(a[v][i]) )
				maior = v;
		}
		if(i != maior) {
			tmp = ordemLinhas[i];
			ordemLinhas[i] = ordemLinhas[maior];
			ordemLinhas[maior] = tmp;
			trocaLinhas(a,i,maior,n);
			#ifdef DEBUG
			fprintf(out,"Troca linhas %d %d\n",i,maior);
			#endif
		}
		// +++++++++++++++++++++++++ */
	

		for(j = i+1;j<n;j++) {
			m = a[j][i]/a[i][i];
			a[j][i] = m;
			for(k=i+1;k<n;k++)
				a[j][k] = a[j][k] -m*a[i][k];
		}

	}

}


/**
 * @brief      Calcula a inversa da matriz \p a[][] de tamanho \p n com as matrizes LU (que estao armazenadas em \p a[][]) e o vetor \p ordemLinhas[]
 *
 * @param      aInv         Ponteiro do endereço de memória que será armazenada a inversa
 * @param      a            Matriz que será calculada a inversa
 * @param      y            não sei pedro plz fix
 * @param      ordemLinhas  Ponteiro para vetor que tem a ordem das linhas após o pivotamento
 * @param[in]  n            Tamanho da matriz
 */
void calculaInversa(double **aInv,double **a,double *y,int *ordemLinhas,int n) {
	int k,i,j;
	double soma;
	for(k=0;k<n;k++) {

		y[ 0 ] = ( ordemLinhas[k] == 0); // /L(a,0,0);
		for(i = 1;i<n;i++) {
			soma = ( ordemLinhas[k] == i);
			for(j = 0;j<i;j++)
				soma -=  a[i][j]*y[j]; // L(a,i,j)*y[j];
			
			y[ i ] = soma; // /L(a,i,i);
		}

		#ifdef DEBUG
		fprintf(out,"Vetor y:\n");
		for(i=0;i<n;i++)
			fprintf(out,"%.17g ",y[i]);
		fprintf(out,"\n\n");
		#endif

			
		aInv[n-1][ ordemLinhas[ordemLinhas[k]] ] = y[n-1]/a[n-1][n-1];
		for(i=n-2;i>=0;i--) {
			soma = y[i];
			for(j=i+1;j<n;j++)
				soma -= a[i][j]*aInv[j][ ordemLinhas[ordemLinhas[k]] ];
			
			aInv[i][ ordemLinhas[ordemLinhas[k]] ] = soma/a[i][i];
		}
	
	}

}


/**
 * @brief      Faz o refinamento da matriz inversa \p aInv[][], coluna por coluna
 *
 * @param      out          Arquivo de saída dos dados
 * @param      a            Ponteiro para a matriz original
 * @param      aInv         Ponteiro para a matriz inversa
 * @param      aOrig        Ponteiro para a matriz original ?? fix pedro
 * @param      ordemLinhas  Ponteiro para vetor que tem a ordem das linhas após o pivotamento
 * @param[in]  n            Tamanho da matriz
 * @param[in]  e_k          Número de iterações do refinamento a serem executadas
 */
void refinamento(FILE *out,double **a,double **aInv,double **aOrig,int *ordemLinhas,int n,int e_k) {
	int i,k,v,j;

	double soma;
	double *r = (double *)malloc(n*sizeof(double));
	double *w = (double *)malloc(n*sizeof(double));
	double *y = (double *)malloc(n*sizeof(double));

	for(k=0;k<n;k++) {
		for(v = 0;v < e_k;v++) {
			// r sera o residuo
			// r = b - A*x 
			for(i=0; i<n; i++) {
				soma = 0;
				for(j=0;j<n;j++) {
					soma += aOrig[ ordemLinhas[i] ][j]*aInv[j][ ordemLinhas[k] ];
				}
				r[i] = ( k == i )-soma;
			}

			// 
			// A*w = r
			//
			y[0] = r[0]; // /L(a,0,0);
			for(i = 1;i<n;i++) {
				soma = r[i];
				for(j = 0;j<i;j++) {
					soma -= a[i][j]*y[j]; //L(a,i,j)*y[j]
				}						

				y[i] = soma; // /L(a,i,i);			
			}

			// +++ Subs retroativa +++
			w[n-1] = y[n-1]/a[n-1][n-1];
			for(i = n-2;i >= 0;i--) {
				soma = y[i];
				for(j= i+1;j < n;j++) {
					soma = soma - a[i][j]*w[j];
				}
				w[i] = soma/a[i][i];
			}
			// +++++++++++++++++++++++

			// x = x + w
			// ++ Obter a prox solucao x ++
			for(i = 0;i< n;i++) {
				aInv[i][ ordemLinhas[k] ] =aInv[i][ ordemLinhas[k] ] + w[i];
			}

			fprintf(out,"# iter %d: \t <||%.17g||>\n", v+1, normaVetor(r,n));
		}
		
	}

}

int main(int argc, char *argv[]) {

    int e_n =0, e_k = 0;
	FILE *in, *out;

	leEntrada(argc,argv,&in,&out,&e_n,&e_k);

	int n,i,j;
	
	if(e_n==0)
		fscanf(in,"%d",&n);
	else
		n=e_n;

	double **aOrig = alocaMatriz(n);
	double **a = alocaMatriz(n);
	double *y = (double *)malloc(n*sizeof(double));
	int *ordemLinhas = (int *)malloc(n*sizeof(int));
	double **aInv = alocaMatriz(n);
	double tempoIni,tempoFim;

	if(e_n==0)
		leMatriz(in,a,n);
	else
		generateSquareRandomMatrix(a,n);

	#ifdef DEBUG
	fprintf(out,"Matriz:\n");	
	imprimeMatriz(out,a,n);
	fprintf(out,"\n\n");
	#endif

	for(i = 0;i<n;i++) {
		ordemLinhas[i] = i;		
		for(j = 0;j<n;j++) {
			aOrig[i][j] = a[i][j];
		}
	}

	tempoIni = timestamp();
	// ++++ Metodo de Gauss e l ++++
	metodoDeGaussPivotamento(a,n,ordemLinhas);
	// ++++++++++++++++++++++++++++++++
	tempoFim = timestamp();
	fprintf(out,"Tempo LU: %.17g\n",tempoFim-tempoIni);

	if( fabs(a[n-1][n-1]) < 0.000000001 ) {
		fprintf(out,"Matriz nao inversivel.\n");
		return -1;
	}
	
	tempoIni = timestamp();
	// +++ Resolve os sistemas com a L +++
	calculaInversa(aInv,a,y,ordemLinhas,n);
	// ==================================
	tempoFim = timestamp();
	fprintf(out,"Tempo iter: %.17g\n",tempoFim-tempoIni);


	#ifdef DEBUG
	fprintf(out,"Matriz original:\n\n");
	for(i=0;i<n;i++) {
		for(j=0;j<n;j++)
			fprintf(out,"%.17g ",aOrig[i][ j ]);
		fprintf(out,"\n");	
	}
	fprintf(out,"\n\n");
	#endif

	tempoIni = timestamp();
	// ====== Refinamento =============i
	refinamento(out,a,aInv,aOrig,ordemLinhas,n,e_k);
	// +++++++++++++++++++++
	tempoFim = timestamp();
	fprintf(out,"Tempo residuo: %.17g\n",tempoFim-tempoIni);
	
	fprintf(out,"#\n%d\n",n);
	for(i=0;i<n;i++) {
		for(j=0;j<n;j++)
			fprintf(out,"%.17g \t",aInv[i][ j ]);
		fprintf(out,"\n");	
	}
	fprintf(out,"\n\n");
	
	#ifdef DEBUG
	fprintf(out,"A x Ainv =\n\n");
	a = multiplicaMatriz(aOrig,aInv,n);
	imprimeMatriz(out,a,n);
	#endif
	
	return 0;
}




