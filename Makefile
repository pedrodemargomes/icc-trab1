FLAGS = -Wall -lm -O3

all: invmat

clean:
	rm -f *.o invmat

invmat:
	gcc invmat.c $(FLAGS) -o invmat

doc:
	doxygen

